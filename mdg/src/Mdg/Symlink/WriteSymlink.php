<?php
namespace Mdg\Symlink;

use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Filesystem\Directory\WriteInterface;

class WriteSymlink
{
    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    private $driver;
    public function __construct(
        \Magento\Framework\Filesystem\Driver\File $driver
    ) {
        $this->driver = $driver;
    }
    /**
     * Creates symlink on a file and places it to destination
     *
     * @param string $absoluteSourcePath
     * @param string $absoluteDestinationPath
     * @param bool $useRelativePath [optional]
     * @return bool
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws ValidatorException
     */
    public function createSymlink($absoluteSourcePath, $absoluteDestinationPath, $useRelativePath = true)
    {
        $parentDirectory = $this->driver->getParentDirectory($absoluteDestinationPath);
        if (!$this->driver->isExists($parentDirectory)) {
            $this->driver->createDirectory($parentDirectory);
        }
        $sourcePath = $absoluteSourcePath;
        if ($useRelativePath) {
            $sourcePath = $this->getDependedRelativePath($absoluteDestinationPath, $absoluteSourcePath);
        }

        return $this->driver->symlink($sourcePath, $absoluteDestinationPath, $this->driver);
    }

    /**
     * @param string $from
     * @param string $to
     * @return string
     */
    protected function getDependedRelativePath($from, $to)
    {
        // some compatibility fixes for Windows paths
        $from = str_replace('\\', '/', $from);
        $to   = str_replace('\\', '/', $to);

        $from     = explode('/', $from);
        $to       = explode('/', $to);
        $relPath  = $to;

        foreach ($from as $depth => $dir) {
            // find first non-matching dir
            if ($dir === $to[$depth]) {
                // ignore this directory
                array_shift($relPath);
            } else {
                // get number of remaining dirs to $from
                $remaining = count($from) - $depth;
                if ($remaining > 1) {
                    // add traversals up to first matching dir
                    $padLength = (count($relPath) + $remaining - 1) * -1;
                    $relPath = array_pad($relPath, $padLength, '..');
                    break;
                } else {
                    $relPath[0] = './' . $relPath[0];
                }
            }
        }
        return implode('/', $relPath);
    }
}
