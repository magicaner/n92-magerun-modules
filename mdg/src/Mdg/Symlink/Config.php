<?php
namespace Mdg\Symlink;

class Config extends \Magento\Framework\DataObject
{
    /**
     * @var \Magento\Framework\ObjectManager\ObjectManager
     */
    private $objectManager;

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    private $directoryList;

    /**
     * @var \Magento\Framework\Filesystem\Directory\Read
     */
    private $fileSystemReader;

    /**
     * @var \Symfony\Component\Yaml\Yaml
     */
    private $yaml;

    public function __construct(
        \Magento\Framework\ObjectManager\ObjectManager $objectManager,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Directory\ReadFactory $readFactory,
        \Symfony\Component\Yaml\Yaml $yaml
    ) {
        $this->directoryList = $directoryList;
        $this->fileSystemReader = $readFactory->create($this->directoryList->getRoot() . '/..');
        $this->yaml = $yaml;
        $this->loadConfig();

    }

    protected function loadConfig()
    {
        $configFileName = 'env.yml';
        if (!$this->fileSystemReader->isExist($configFileName)) {
            throw new \Exception('env.yml not found');
        }
        $configFileContent = $this->fileSystemReader->readFile($configFileName);

        $config = $this->yaml->parse($configFileContent);

        $this->setData($config);
    }
}
