<?php
namespace Mdg\Symlink;

use Magento\Framework\Filesystem\Directory\DenyListPathValidator;
use Magento\Framework\Filesystem\DriverPool;
use Magento\Framework\Filesystem\Directory\PathValidator;

class WriteFactory extends \Magento\Framework\Filesystem\Directory\WriteFactory
{
    /**
     * Pool of filesystem drivers
     *
     * @var DriverPool
     */
    private $driverPool;

    /**
     * Deny List Validator
     *
     * @var DenyListPathValidator
     */
    private $denyListPathValidator;

    /**
     * Constructor
     *
     * @param DriverPool $driverPool
     * @param DenyListPathValidator|null $denyListPathValidator
     */
    public function __construct(DriverPool $driverPool, ?DenyListPathValidator $denyListPathValidator = null)
    {
        $this->driverPool = $driverPool;
        $this->denyListPathValidator = $denyListPathValidator;
    }

    /**
     * Create a writable directory
     *
     * @param string $path
     * @param string $driverCode
     * @param int $createPermissions
     * @param string $directoryCode
     * @return \Magento\Framework\Filesystem\Directory\Write
     */
    public function create($path, $driverCode = DriverPool::FILE, $createPermissions = null, $directoryCode = null)
    {
        $driver = $this->driverPool->getDriver($driverCode);
        $factory = new \Magento\Framework\Filesystem\File\WriteFactory(
            $this->driverPool
        );

        return new Write(
            $factory,
            $driver,
            $path,
            $createPermissions,
            new PathValidator($driver)
        );
    }
}
