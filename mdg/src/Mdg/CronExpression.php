<?php
namespace Mdg;


use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\App\State as AppState;
use Magento\Framework\Setup\ConsoleLogger;
use Magento\Framework\Setup\Declaration\Schema\DryRunLogger;
use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

/**
 * Command to read cron expression by its name
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CronExpression extends AbstractMagentoCommand
{
    const ARGUMENT_NAME = 'name';
    /**
     * @var DeploymentConfig
     */
    private $deploymentConfig;

    /**
     * @var AppState
     */
    private $appState;
    /**
     * @var ServiceManager
     */
    private $serviceManager;

    /**
     * @var \Magento\Cron\Model\Config
     */
    private $cronConfig;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function init(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->detectMagento($output);
        $this->initMagento();
        
        $objectManager = $this->getApplication()->getObjectManager();
        
        $this->deploymentConfig = $objectManager->get(DeploymentConfig::class);
        $this->appState = $objectManager->get(AppState::class);
        $this->cronConfig = $objectManager->get(\Magento\Cron\Model\Config::class);
        $this->scopeConfig = $objectManager->get(ScopeConfigInterface::class);

    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $options = [
            new InputArgument(
                'name',
                InputArgument::REQUIRED,
                'Name of cron joob'
            )
        ];
        $this->setName('mdg:cron:expression:show')
            ->setDescription('Print cron expression')
            ->setDefinition($options);
        parent::configure();
    }

    /**
     * @return ServiceManager
     */
    protected function iniSetupModuleServiceManager()
    {
        $configuration = include BP . '/setup/config/application.config.php';

        $managerConfig = isset($configuration['service_manager']) ? $configuration['service_manager'] : [];
        $managerConfig = new ServiceManagerConfig($managerConfig);

        $this->serviceManager = new ServiceManager();
        $managerConfig->configureServiceManager($this->serviceManager);
        $this->serviceManager->setService('ApplicationConfig', $configuration);
        $this->serviceManager->get('ModuleManager')->loadModules();

        $bootstrapApplication = new \Magento\Setup\Application();
        $application = $bootstrapApplication->bootstrap($configuration);
        $this->serviceManager = $application->getServiceManager();
        return $this->serviceManager;
    }

    /**
     * @return ServiceManager
     */
    protected function getServiceManager()
    {
        if (!$this->serviceManager) {
            $this->iniSetupModuleServiceManager();
        }

        return $this->serviceManager;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);
        $jobName = $input->getArgument(self::ARGUMENT_NAME);
        try {
            $jobs = $this->cronConfig->getJobs();
            $foundJob = null;
            foreach ($jobs as $group) {
                foreach ($group as $name => $job) {
                    if ($name == $jobName) {
                        $foundJob = $job;
                        break;
                    }
                }
            }
            if (!$foundJob) {
                $output->writeln("<error>Job $jobName is not found");
            }
            $expression = $this->getCronExpression($foundJob);
            $output->writeln(
                "<info>Expression is $expression</info>"
            );

            $output->writeln(
                '<info>Done</info>'
            );
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }



        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }

    /**
     * Get cron expression of cron job.
     *
     * @param array $jobConfig
     * @return null|string
     */
    protected function getCronExpression($jobConfig)
    {
        $cronExpression = null;
        if (isset($jobConfig['config_path'])) {
            $cronExpression = $this->getConfigSchedule($jobConfig) ?: null;
        }

        if (!$cronExpression) {
            if (isset($jobConfig['schedule'])) {
                $cronExpression = $jobConfig['schedule'];
            }
        }
        return $cronExpression;
    }

    /**
     * Get config of schedule.
     *
     * @param array $jobConfig
     * @return mixed
     */
    protected function getConfigSchedule($jobConfig)
    {
        $cronExpr = $this->scopeConfig->getValue(
            $jobConfig['config_path'],
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return $cronExpr;
    }
}
