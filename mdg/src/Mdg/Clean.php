<?php

namespace Mdg;

use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;
use function Univar\hash;
// convert below code to php 7.3 version



class Clean extends AbstractMagentoCommand
{
    /**
     * @var \Mdg\Symlink\Config
     */
    private $config;

    /**
     * @var \Mdg\Symlink\WriteSymlink
     */
    private $writeSymlink;
    /**
     * @var \Magento\Framework\Filesystem\Directory\Write
     */
    private $fileSystemWriter;

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    private $directoryList;

    /**
     * @var \Magento\Framework\Filesystem\Directory\Read
     */
    private $fileSystemReader;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    private $fileSystem;
    protected function init(
        InputInterface  $input,
        OutputInterface $output
    ) {
        $this->detectMagento($output);
        $this->initMagento();

        $objectManager = $this->getApplication()->getObjectManager();
        $this->config = $objectManager->get(\Mdg\Symlink\Config::class);
        $this->directoryList = $objectManager->get(\Magento\Framework\Filesystem\DirectoryList::class);
        $this->writeSymlink = $objectManager->get(\Mdg\Symlink\WriteSymlink::class);
        $this->fileSystemWriter = $objectManager->get(\Magento\Framework\Filesystem\Directory\WriteFactory::class)->create($this->directoryList->getRoot());
        $this->fileSystemReader = $objectManager->get(\Magento\Framework\Filesystem\Directory\ReadFactory::class)->create($this->directoryList->getRoot());
        $this->fileSystem = $objectManager->get(\Magento\Framework\Filesystem\Driver\File::class);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('mdg:clean')
            ->setDescription('Clean frontend compiled files');
        parent::configure();
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $paths = $this->config->getData('clean/paths');

        $output->writeln("<info>Clean paths</info>");
        $magentoRoot = $this->directoryList->getRoot() . '/';
        $projectRoot = $magentoRoot . '../';

        foreach ($paths as $path) {
            $output->write("<info>Clean '$path'</info>");
            $magentoPath = $this->fileSystem->getRealPathSafety($this->fileSystem->getAbsolutePath($magentoRoot, $path));
            if (!$this->fileSystem->isExists($magentoPath)) {
                // ignore missing paths
                $output->writeln("<error> - ignored, source path not exists.</error>");
                continue;
            }

            $this->fileSystemWriter->delete($path);
            $output->writeln("<info> - cleaned.</info>");
        }

        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }

}
