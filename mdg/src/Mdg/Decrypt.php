<?php
namespace Mdg;


use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\App\State as AppState;
use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

/**
 * Command to decrypt given value and print
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Decrypt extends AbstractMagentoCommand
{
    const ARGUMENT_STRING = 'path';
    /**
     * @var DeploymentConfig
     */
    private $deploymentConfig;

    /**
     * @var AppState
     */
    private $appState;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    private $encryptor;
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function init(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->detectMagento($output);
        $this->initMagento();
        
        $objectManager = $this->getApplication()->getObjectManager();
        
        $this->deploymentConfig = $objectManager->get(DeploymentConfig::class);
        $this->appState = $objectManager->get(AppState::class);
        $this->scopeConfig = $objectManager->get(ScopeConfigInterface::class);
        $this->encryptor = $objectManager->get(\Magento\Framework\Encryption\EncryptorInterface::class);

    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $options = [
            new InputArgument(
                self::ARGUMENT_STRING,
                InputArgument::REQUIRED,
                'String to decrypt'
            ),
        ];
        $this->setName('mdg:config:decrypt')
            ->setDescription('Decrypt given string')
            ->setDefinition($options);
        parent::configure();
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);
        $value = $input->getArgument(self::ARGUMENT_STRING);
        try {
            $value = $this->encryptor->decrypt($value);
            $output->writeln("<info>Decrypted value is: $value</info>");
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }
        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }
}
