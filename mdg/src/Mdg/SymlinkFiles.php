<?php

namespace Mdg;

use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;
use function Univar\hash;

class SymlinkFiles extends AbstractMagentoCommand
{
    /**
     * @var \Mdg\Symlink\Config
     */
    private $config;

    /**
     * @var \Mdg\Symlink\WriteSymlink
     */
    private $writeSymlink;

    /**
     * @var \Magento\Framework\Filesystem\Directory\Write
     */
    private $fileSystemWriter;

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    private $directoryList;

    /**
     * @var \Magento\Framework\Filesystem\Directory\Read
     */
    private $fileSystemReader;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    private $fileSystem;
    protected function init(
        InputInterface  $input,
        OutputInterface $output
    ) {
        $this->detectMagento($output);
        $this->initMagento();

        $objectManager = $this->getApplication()->getObjectManager();
        $this->config = $objectManager->get(\Mdg\Symlink\Config::class);
        $this->directoryList = $objectManager->get(\Magento\Framework\Filesystem\DirectoryList::class);
        $this->writeSymlink = $objectManager->get(\Mdg\Symlink\WriteSymlink::class);
        $this->fileSystemWriter = $objectManager->get(\Magento\Framework\Filesystem\Directory\WriteFactory::class)->create($this->directoryList->getRoot());
        $this->fileSystemReader = $objectManager->get(\Magento\Framework\Filesystem\Directory\ReadFactory::class)->create($this->directoryList->getRoot());
        $this->fileSystem = $objectManager->get(\Magento\Framework\Filesystem\Driver\File::class);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('mdg:symlinks:create')
            ->setDescription('Create symlinks by env.yml');
        parent::configure();
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $symlinks = $this->config->getData('symlinks');

        $output->writeln("<info>Create syminks</info>");
        $magentoRoot = $this->directoryList->getRoot() . '/';
        $projectRoot = $magentoRoot . '../';

        foreach ($symlinks as $path => $symlink) {
            $output->write("<info>Creating symink for '$symlink'</info>");
            $sourcePath = $this->fileSystem->getRealPathSafety($this->fileSystem->getAbsolutePath($projectRoot, $path));
            if (!$this->fileSystem->isExists($sourcePath)) {
                // ignore missing paths
                $output->writeln("<info> - ignored, source path not exists.</info>");
                continue;
            }

            $destinationPath = $this->fileSystem->getRealPathSafety($this->fileSystem->getAbsolutePath($magentoRoot, $symlink));

            $this->fileSystemWriter->delete($destinationPath);
            $this->writeSymlink->createSymlink($sourcePath, $destinationPath);
            $output->writeln("<info> - created.</info>");
        }

        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }

}
