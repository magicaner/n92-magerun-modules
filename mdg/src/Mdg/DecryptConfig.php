<?php
namespace Mdg;


use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\App\State as AppState;
use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

/**
 * Command to read config value, decrypt and print
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class DecryptConfig extends AbstractMagentoCommand
{
    const ARGUMENT_PATH = 'path';
    const OPTION_SCOPE = 'scope';
    const OPTION_SCOPE_ID = 'scope_id';
    /**
     * @var DeploymentConfig
     */
    private $deploymentConfig;

    /**
     * @var AppState
     */
    private $appState;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    private $encryptor;
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function init(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->detectMagento($output);
        $this->initMagento();
        
        $objectManager = $this->getApplication()->getObjectManager();
        
        $this->deploymentConfig = $objectManager->get(DeploymentConfig::class);
        $this->appState = $objectManager->get(AppState::class);
        $this->scopeConfig = $objectManager->get(ScopeConfigInterface::class);
        $this->encryptor = $objectManager->get(\Magento\Framework\Encryption\EncryptorInterface::class);

    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $options = [
            new InputArgument(
                self::ARGUMENT_PATH,
                InputArgument::REQUIRED,
                'Variable path'
            ),
            new InputOption(
                self::OPTION_SCOPE,
                null,
                InputOption::VALUE_OPTIONAL,
                'Scope name, eg: websites, stores, default',
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT
            ),
            new InputOption(
                self::OPTION_SCOPE_ID,
                null,
                InputOption::VALUE_OPTIONAL,
                'Scope name, eg: websites, stores, default'
            ),
        ];
        $this->setName('mdg:config:show-decrypted')
            ->setDescription('Decrypt and print config value ')
            ->setDefinition($options);
        parent::configure();
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);
        $configPath = $input->getArgument(self::ARGUMENT_PATH);
        $configScope = $input->getOption(self::OPTION_SCOPE);
        $configScopeId = $input->getOption(self::OPTION_SCOPE_ID);
        try {
            $value = $this->getConfigValue($configPath, $configScope, $configScopeId);
            $value = $this->encryptor->decrypt($value);
            $output->writeln("<info>Config value is: $value</info>");
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }
        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }

    /**
     * get config value
     *
     * @param string $variablePath
     * @param string $scope
     * @param string $scopeId
     * @return mixed
     */
    protected function getConfigValue($variablePath, $scope = null, $scopeId = null)
    {
        return $this->scopeConfig->getValue(
            $variablePath,
            $scope,
            $scopeId
        );
    }
}
