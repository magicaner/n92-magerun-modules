<?php
namespace Mdg;

use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Command for runing patches manually
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PatchApplierCommand extends AbstractMagentoCommand
{
    /**
     * @var \Magento\Framework\Setup\Patch\PatchApplierFactory
     */
    private $applierFactory;
    
    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    private $moduleList;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function init(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->detectMagento($output);
        $this->initMagento();
        
        $objectManager = $this->getApplication()->getObjectManager();
        
        $this->moduleList =
            $objectManager->get(\Magento\Framework\Module\ModuleListInterface::class);
        $this->applierFactory =
            $objectManager->get(\Magento\Framework\Setup\Patch\PatchApplierFactory::class);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $options = [];
        $this->setName('mdg:patch:data:apply')
            ->setDescription('Apply Data Patches')
            ->setDefinition($options);
        parent::configure();
    }
    
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->init($input, $output);
        
        $outputFormatter = $output->getFormatter();
        $outputFormatter->setStyle('detail', new OutputFormatterStyle('blue'));

        try {
            /** @var \Magento\Framework\Setup\Patch\PatchApplier $patchApplier */
            $patchApplier = $this->applierFactory->create();
            $output->writeln('<detail>Apply patches</detail>');
            $moduleNames = $this->moduleList->getNames();
            foreach ($moduleNames as $moduleName) {
                $output->writeln('<detail>' . $moduleName .'</detail>');
                $patchApplier->applyDataPatch($moduleName);
            }

            $output->writeln('<info>Done</info>');
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }

        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }
}
