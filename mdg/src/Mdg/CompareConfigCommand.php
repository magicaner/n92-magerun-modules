<?php

namespace Mdg;

use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Model\ResourceModel\Type\Db\ConnectionFactory;
use Magento\Framework\Model\ResourceModel\Type\Db\ConnectionFactoryInterface;

class CompareConfigCommand extends AbstractMagentoCommand
{
    const TABLE_NAME = 'core_config_data';

    /**
     * @var ConnectionFactory
     */
    private $connectionFactory;
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var \Magento\Framework\DB\Adapter\Pdo\Mysql
     */
    protected $remoteConnection;

    /**
     * @var \Magento\Framework\DB\Adapter\Pdo\Mysql
     */
    protected $localConnection;

    protected function configure()
    {
        $this
            ->setName('mdg:config:compare')
            ->setDescription('Compare local config with remote, and create diff sql query');

        $this->setDefinition(
            [
                new InputOption(
                    '--dbname',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Database Name'
                ),
                new InputOption(
                    '--dbuser',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Database Username',
                    'root'
                ),
                new InputOption(
                    '--dbhost',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Database Host',
                    'localhost'
                ),
                new InputOption(
                    '--dbpassword',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Database Passsword',
                    'pass'
                )
            ]
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int|void
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->init($input, $output);

        $output->writeln('<info>Find differencies</info>');
        $output->writeln('');

        $remoteData = $this->getRemoteConfigData();
        $localData = $this->getLocalConfigData();
        $rows = $this->compareRows($remoteData, $localData);
        $sqlQueries = $this->generateSqlQueries($rows);

        foreach ($sqlQueries as $query) {
            $output->writeln('<info>' . $query . '</info>');
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function init(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->detectMagento($output);
        $this->initMagento();

        $objectManager = $this->getApplication()->getObjectManager();

        $this->connectionFactory =
            $objectManager->get('Magento\Framework\Model\ResourceModel\Type\Db\ConnectionFactory');
        $this->resource =
            $objectManager->get('Magento\Framework\App\ResourceConnection');


        $this->remoteConnection = $this->createConnection(
            $input->getOption('dbname'),
            $input->getOption('dbhost'),
            $input->getOption('dbuser'),
            $input->getOption('dbpassword')
        );

        $this->localConnection = $this->resource
            ->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
    }

    /**
     * Create new connection
     *
     * @param string $dbname
     * @param string $host
     * @param string $username
     * @param string $password
     * @return \Magento\Framework\DB\Adapter\Pdo\Mysql
     */
    protected function createConnection(
        $dbname,
        $host,
        $username,
        $password
    ) {
        $connectionConfig = [
            'dbname' => $dbname,
            'host' => $host,
            'username' => $username,
            'password' => $password
        ];

        return $this->connectionFactory->create($connectionConfig);
    }

    /**
     * Get all data from local table
     *
     * @return array
     */
    protected function getLocalConfigData()
    {
        $select = $this->localConnection->select();
        $select->from(self::TABLE_NAME);

        $rows = $this->localConnection->fetchAll($select);
        $rowsByKey = [];
        foreach ($rows as $row) {
            $rowsByKey[$row['path'] . ':' . $row['scope'] . ':' . $row['scope_id']] = $row;
        }

        return $rowsByKey;
    }

    /**
     * Get all data fore remote table
     *
     * @return array
     */
    protected function getRemoteConfigData()
    {
        $select = $this->remoteConnection->select();
        $select->from(self::TABLE_NAME);

        $rows = $this->remoteConnection->fetchAll($select);
        $rowsByKey = [];
        foreach ($rows as $row) {
            $rowsByKey[$row['path'] . ':' . $row['scope'] . ':' . $row['scope_id']] = $row;
        }

        return $rowsByKey;
    }

    /**
     * Compare remote data with local
     * return different rows from remote:
     *
     * @param $remoteRows
     * @param $localRows
     */
    protected function compareRows(
        $remoteRows,
        $localRows
    ) {
        $differentRows = [];
        foreach ($remoteRows as $key => $remoteRow) {
            if (!isset($localRows[$key])) {
                $differentRows[$key] = $remoteRow;
                continue;
            }

            $localRow = $localRows[$key];

            if ($difference = array_diff($remoteRow, $localRow)) {
                $differentRows[$key] = $remoteRow;
            }
        }

        return $differentRows;
    }

    /**
     * Generate sql queries
     *
     * @param array $rows
     * @return array
     */
    protected function generateSqlQueries($rows)
    {
        $sqlQueries = [];
        $pattern = 'INSERT core_config_data SET' .
            ' `path` = \'%s\'' .
            ' `value` = \'%s\',' .
            ' `scope` = \'%s\',' .
            ' `scope_id` = \'%s\',' .
            ' ON DUPLICATE KEY UPDATE `value` = \'%s\';';
        foreach ($rows as $row) {
            $sqlQueries[] = sprintf(
                $pattern,
                $row['path'],
                $row['value'],
                $row['scope'],
                $row['scope_id'],
                $row['value']
            );
        }

        return $sqlQueries;
    }


}
