<?php

namespace Mdg;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\App\State as AppState;
use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;
use function Univar\hash;

class Test extends AbstractMagentoCommand
{
    /**
     * @var DeploymentConfig
     */
    private $deploymentConfig;

    /**
     * @var AppState
     */
    private $appState;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    private $encryptor;
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function init(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->detectMagento($output);
        $this->initMagento();
        
        $objectManager = $this->getApplication()->getObjectManager();
        
        $this->deploymentConfig = $objectManager->get(DeploymentConfig::class);
        $this->appState = $objectManager->get(AppState::class);
        $this->scopeConfig = $objectManager->get(ScopeConfigInterface::class);
        $this->encryptor = $objectManager->get(\Magento\Framework\Encryption\EncryptorInterface::class);

    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('mdg:test')
            ->setDescription('Testing code');
        parent::configure();
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $url = 'https://cert-xiecomm.paymetric.com/diecomm/AccessToken';
        $sharedKey = 'kZ?4!3YaKr7_5%eXn9A$=x8R2Db{y/B6';
        $guid = '34d39580-9a88-4f69-ba83-2f9de563383d';

        $objectManager = $this->getApplication()->getObjectManager();
        $httpClient = $objectManager->get(\Magento\Framework\HTTP\ZendClient::class);


        $timestamp = (new \DateTime())->format(\DateTime::ISO8601);
        $nonce = \hash('sha256', (string)time());


        $xml = <<<XML
            <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><AjaxPacketModel/>
            XML;

        $signatureElements = [
            $xml
        ];

        $signature = base64_encode(hash_hmac('sha256', $xml, $sharedKey, true));
        //$signature = \hash('sha256', base64_encode(strtoupper(implode('|', $signatureElements))));

        $httpClient
            ->setUri($url)
            ->setMethod('POST')
            ->setParameterPost('MerchantGuid', $guid)
            ->setParameterPost('SessionRequestType', '2')
            ->setParameterPost('Signature', $signature)
            ->setParameterPost('MerchantDevelopmentEnvironment', 'php')
            ->setParameterPost('Packet', $xml)
        ;

        $result = $httpClient->request();


        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }
}
