<?php
namespace Mdg;


use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\App\State as AppState;
use Magento\Framework\Setup\ConsoleLogger;
use Magento\Framework\Setup\Declaration\Schema\DryRunLogger;
use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

/**
 * Command for updating config.php.
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UpdateConfigCommand extends AbstractMagentoCommand
{
    /**
     * @var DeploymentConfig
     */
    private $deploymentConfig;

    /**
     * @var AppState
     */
    private $appState;
    /**
     * @var ServiceManager
     */
    private $serviceManager;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function init(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->detectMagento($output);
        $this->initMagento();
        
        $objectManager = $this->getApplication()->getObjectManager();
        
        $this->deploymentConfig = $objectManager->get(DeploymentConfig::class);
        $this->appState = $objectManager->get(AppState::class);
        
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $options = [
            new InputOption(
                'dry-run',
                null,
                InputOption::VALUE_OPTIONAL,
                'Magento Installation will be run in dry-run mode',
                false
            )
        ];
        $this->setName('mdg:setup:update:config')
            ->setDescription('Upgrades the Magento application, DB data, and schema')
            ->setDefinition($options);
        parent::configure();
    }

    /**
     * @return ServiceManager
     */
    protected function iniSetupModuleServiceManager()
    {
        $configuration = include BP . '/setup/config/application.config.php';

        $managerConfig = isset($configuration['service_manager']) ? $configuration['service_manager'] : [];
        $managerConfig = new ServiceManagerConfig($managerConfig);

        $this->serviceManager = new ServiceManager();
        $managerConfig->configureServiceManager($this->serviceManager);
        $this->serviceManager->setService('ApplicationConfig', $configuration);
        $this->serviceManager->get('ModuleManager')->loadModules();

        $bootstrapApplication = new \Magento\Setup\Application();
        $application = $bootstrapApplication->bootstrap($configuration);
        $this->serviceManager = $application->getServiceManager();
        return $this->serviceManager;
    }

    /**
     * @return ServiceManager
     */
    protected function getServiceManager()
    {
        if (!$this->serviceManager) {
            $this->iniSetupModuleServiceManager();
        }

        return $this->serviceManager;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);
        try {
            $installerFactory = $this->getServiceManager()->get(\Magento\Setup\Model\InstallerFactory::class);

            /** @var \Magento\Setup\Model\Installer $installer */
            $installer = $installerFactory->create(new ConsoleLogger($output));
            $output->writeln(
                '<info>Update etc/config.php</info>'
            );
            $installer->updateModulesSequence(true);

            $output->writeln(
                '<info>Done</info>'
            );
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }



        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }
}
